#include <iostream>
#include <vector>
#include "NeuralNetwork.h"

using namespace std;

extern "C" int SaveWeights(void* network, char* dir)
{
	Network& net = *(Network*)network;
	ofstream fout(dir);

	if (fout.is_open() == false)
		return 0;

	for (int i = 0; i < net.layers.size(); i++)
		for (int j = 0; j < net.layers[i].size(); j++)
			for (int w = 0; w < net.layers[i][j].weights.size(); w++)
				fout << net.layers[i][j].weights[w] << " ";

	return 1;
}

extern "C" int LoadWeights(void* network, char* dir)
{
	Network& net = *(Network*)network;
	ifstream fin(dir);

	if (fin.is_open() == false)
		return 0;

	for (int i = 0; i < net.layers.size(); i++)
		for (int j = 0; j < net.layers[i].size(); j++)
			for (int w = 0; w < net.layers[i][j].weights.size(); w++)
				fin >> net.layers[i][j].weights[w];

	return 1;
}

void Print(vector<double> vec)
{
	for (int i = 0; i < vec.size(); i++)
		cout << vec[i] << " ";

	cout << endl;
}

extern "C" void Train(void* network, double minError)
{
	Network& net = *(Network*)network;

	for (int i = 0; net.error > minError; i++)
	{
		if (i % 1000 == 0)
			cout << net.error << endl;

		net.Train();
	}
}

extern "C" void* InitializeFromFile(char* dir, int* numOutputs)
{
	ifstream fin(dir);

	if (fin.is_open() == false)
	{
		cout << "File not found!" << endl;
		return 0;
	}

	string arch;
	std::getline(fin, arch);

	Network* net = new Network(arch);

	vector<vector<double> > inputs;
	vector<vector<double> > expected;

	*numOutputs = net->layers.back().size() - 1;

	while (fin.eof() == false)
	{
		string temp;
		std::getline(fin, temp);

		stringstream ss(temp);
		double val;
		vector<double> vec;

		for (int i = 0; i < net->layers.front().size() - 1; i++)		// exclude bias
		{
			ss >> val;
			vec.push_back(val);
		}
		inputs.push_back(vec);
		vec.clear();

		for (int i = 0; i < net->layers.back().size() - 1; i++)
		{
			ss >> val;
			vec.push_back(val);
		}
		expected.push_back(vec);
	}

	net->Initiate(inputs, expected);
	return net;
}

extern "C" void* Initialize(int n, int* data)
{
	string fmt;
	stringstream stream;

	for (int i = 0; i < n; i++)
	{
		string temp;
		stream << data[i];
		stream >> temp;

		fmt += temp;

		if (i < n - 1)
			fmt += " ";
		stream.clear();
	}

	Network* net = new Network(fmt);

	vector<vector<double> > inputs;
	vector<double> temp;
	temp.push_back(0);
	temp.push_back(0);
	inputs.push_back(temp);

	temp[0] = 1;
	temp[1] = 1;
	inputs.push_back(temp);

	vector<vector<double> > outputs;
	vector<double> temp2;
	temp2.push_back(1);
	outputs.push_back(temp2);

	temp2[0] = 0;
	outputs.push_back(temp2);

	cout << "vector ready" << endl;

	net->Initiate(inputs, outputs);

	cout << "Network initialized" << endl;

	return net;
}

extern "C" double* Feed(void* network, double* params, double* outputs)
{
	Network& net = *(Network*)network;

	vector<double> temp;

	int n = net.layers[0].size() - 1;	// exclude bias neuron

	for (int i = 0; i < n; i++)
		temp.push_back(params[i]);

	cout << "End print" << endl;

	vector<double> d = net.Feed(temp);

	for (int i = 0; i < d.size(); i++)	// bias already excluded
		outputs[i] = d[i];

	return outputs;
}